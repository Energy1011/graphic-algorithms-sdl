 /* SDL_Draw.h
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef _SDL_DRAW_
#define _SDL_DRAW_
 
typedef struct _SDL_point
{
	struct _SDL_point *next;
	int x;
	int y;
} SDL_point;


//Internal functions
void SDL_Draw_spline(SDL_Surface *screen, Uint32 color, int x[4], int y[4]);
SDL_point SDL_set_vertex_polygon(int xc, int yc, int r, float radian, float angle);
void SDL_Draw_polygon(SDL_Surface *screen, Uint32 color, SDL_point *points, int sides);
void SDL_Draw_rect (SDL_Surface *screen, Uint32 color, int xi, int yi, int xf, int yf);
void mirror_plot_point_elipse (SDL_Surface *screen, Uint32 color, int xc, int yc, int x, int y);
void SDL_Draw_elipse (SDL_Surface *screen, Uint32 color, int xc, int yc, int rx, int ry);
void mirror_plot_point (SDL_Surface *screen, Uint32 color, int xc, int yc, int x, int y, int r);
void SDL_Draw_circle (SDL_Surface *screen, Uint32 color, int x, int y, int r);
void SDL_Draw_line (SDL_Surface *screen, Uint32 color, int x1, int y1, int x2, int y2);
void SDL_Draw_pixel (SDL_Surface *screen, Uint32 color, int x, int y);

#endif 