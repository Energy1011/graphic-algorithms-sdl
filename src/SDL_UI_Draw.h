 /* SDL_UI_Draw.h
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#ifndef _SDL_UI_DRAW_
#define _SDL_UI_DRAW_

enum {
	YES,
	NO
};

typedef struct
{
  SDL_Surface *toolbox_surface;
  int cols;
  int rows;
  int tile_size;
  int posx, posy;
  int w,h;
  int visible;
} SDL_UI_toolbox;

int SDL_UI_Draw_spline(SDL_Surface *screen, Uint32 color);
int SDL_UI_Draw_polygon(SDL_Surface *screen, Uint32 color, int sides);
void SDL_UI_Set_main_screen(SDL_Surface *screen);
int SDL_UI_MouseOver(int thisposx, int thisposy, int thisw, int thish, int mouseposx, int mouseposy);
void SDL_UI_Drag_toolbox(SDL_Surface *UI_screen, SDL_UI_toolbox *toolbox);
SDL_UI_toolbox* SDL_UI_Create_toolbox(SDL_Surface *screen, Uint32 color, int cols, int rows, int tile_size);
void SDL_UI_Draw_widgets(SDL_Surface *screen);
SDL_Surface* SDL_UI_CreateRGBSurface(SDL_Surface *screen);
int SDL_UI_Draw_rect(SDL_Surface *screen, Uint32 color);
int SDL_UI_Draw_elipse(SDL_Surface *screen, Uint32 color);
int SDL_UI_Draw_circle(SDL_Surface *screen, Uint32 color);
int SDL_UI_Draw_line(SDL_Surface *screen, Uint32 color);

#endif