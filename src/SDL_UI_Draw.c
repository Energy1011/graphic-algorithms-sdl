 /* SDL_UI_Draw.c
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <SDL.h>
#include <math.h>
#include "SDL_Draw.h"
#include "SDL_UI_Draw.h"

SDL_Surface *main_screen;
SDL_UI_toolbox *main_toolbox;

int SDL_UI_Draw_spline(SDL_Surface *screen, Uint32 color){
	SDL_Event event;
	SDL_Surface *tempscreen;
 	tempscreen = SDL_UI_CreateRGBSurface(screen); 
 	// points to spline, 0 = initial, 3 = final, 1,2 = curve mods
 	int x[4], y[4];
 	// step counter to create the spline, 1 and 2 are the rect line, 3 and 4 are the curve point modifiers
 	int step = 1;

 	// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit spline mode 
 							return 0;
 							break;
 					}
				break;

				// Capture initial point to draw the rect line
				case SDL_MOUSEBUTTONDOWN:
					if (step == 1)
					{
						x[0] = event.button.x;
						y[0] = event.button.y;
					}
	 			break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);

						switch (step)
						{
							case 1:
							x[3] = event.motion.x;
							y[3] = event.motion.y;
							SDL_Draw_line(screen, color, x[0], y[0], x[3], y[3]);
							break;

							case 2:
							x[1] = event.motion.x;
							y[1] = event.motion.y;
							x[2] = event.motion.x;
							y[2] = event.motion.y;
							SDL_Draw_spline(screen, color, x, y);
							break;

							
							case 3:
							x[2] = event.motion.x;
							y[2] = event.motion.y;
							SDL_Draw_spline(screen, color, x, y);
							break;

							default:
							break;
						}
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 		break;

	 			// Change next step
	 			case SDL_MOUSEBUTTONUP:
	 				step++;
				break;

				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 		// Finish draw spline
 		if (step > 3)
 		{
 			SDL_FreeSurface(tempscreen);
 			return 0;
 		}
 	} while (1);
}

int SDL_UI_Draw_polygon(SDL_Surface *screen, Uint32 color, int sides){
	int side, e;
    float angle_size,radian;
    int xc, yc;
    int r;
    int angle = 0;

    SDL_point *points;
	
	SDL_Event event;
 	SDL_Surface *tempscreen;
 	tempscreen = SDL_UI_CreateRGBSurface(screen); 

    if (sides < 3 ) sides = 3;

    angle_size = 360 / sides;
    
    radian = 180 / M_PI;  
	
	// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit rect mode 
 							return 1;
 							break;
 					}
					break;

				// Capture initial point to draw the rect
				case SDL_MOUSEBUTTONDOWN:
					xc = event.button.x;
					yc = event.button.y;
	 				break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
				
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);
						// Pythagoras
						r = sqrt(pow((xc - event.motion.x),2) + pow((yc - event.motion.y),2));
						points = (SDL_point *) malloc ((sides + 1) * sizeof (SDL_point));

						for (e = 0; e <= sides; e++){
							// Set the actual vertex
							points[e] = SDL_set_vertex_polygon(xc, yc, r, radian ,angle + ceil(atan2(event.motion.y - yc, event.motion.x - xc) * (180 / M_PI)));
							angle +=angle_size;
						}

    					SDL_Draw_polygon(screen, color, points, sides);
						// Blit canvas screen to main screen
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
							
					}
		 			break;

	 			// Finish draw rect with mouse
	 			case SDL_MOUSEBUTTONUP:
    				if (points != NULL) free(points);
	 				SDL_BlitSurface(screen, NULL, tempscreen, NULL);
					break;

				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 	} while (1);
}

void SDL_UI_Attach_icon(SDL_UI_toolbox *main_toolbox, SDL_Surface *img_toolbar_buttons, int col, int row){

	SDL_Rect rect;
	
	rect.x = main_toolbox->tile_size * col;
	rect.y = main_toolbox->tile_size * row;
	rect.w = 25;
	rect.h = 25;

	SDL_BlitSurface (img_toolbar_buttons, NULL, main_toolbox->toolbox_surface, &rect);

}

void SDL_UI_Set_main_screen(SDL_Surface *screen){
	main_screen = screen;
}

int SDL_UI_MouseOver(int thisposx, int thisposy, int thisw, int thish, int mouseposx, int mouseposy){

	// if to check mouseover
	if ( (mouseposx >= thisposx && mouseposy >= thisposy) && 
		 (mouseposx <= thisposx + thisw  && mouseposy <= thisposy + thish)){
		return 1;
	}else
		return 0;

}

void SDL_UI_Drag_toolbox(SDL_Surface *screen, SDL_UI_toolbox *toolbox)
{
	SDL_Event event;
 	SDL_Surface *tempscreen;
 	tempscreen = SDL_UI_CreateRGBSurface(screen); 
 	int x,y;
 	if (tempscreen == NULL) {
 		printf ("Error Drag_toolbox, Null\n");
 	}

 	// Do it first maybe naver go into do-while (Event loop)
	SDL_GetMouseState(&x, &y);
	toolbox->posx = x;
	toolbox->posy = y;

// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);
						// Blit UI screen to main screen
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 		break;

	 			// Finish dragging toolbox
	 			case SDL_MOUSEBUTTONUP:
		 			SDL_BlitSurface(screen, NULL, tempscreen, NULL);
		 			SDL_FreeSurface(tempscreen);
	 				return;
				break;

				default:
				break;
			}
 		}
		SDL_GetMouseState(&x, &y);
		// check for screen boundaries
		if(x > (main_screen->w - toolbox->w)) x = main_screen->w - toolbox->w;
		if(y > (main_screen->h - toolbox->h)) y = main_screen->h - toolbox->h;
		toolbox->posx = x;
		toolbox->posy = y;
		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay(16);
 	} while (1);
}


SDL_UI_toolbox* SDL_UI_Create_toolbox(SDL_Surface* screen, Uint32 color, int cols, int rows, int tile_size){
	
	SDL_UI_toolbox *toolbox;
	Uint32 color_border_line;

	toolbox = (SDL_UI_toolbox *) malloc (sizeof (SDL_UI_toolbox));

	 toolbox->toolbox_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, cols * tile_size, rows * tile_size, 24, 
	  screen->format->Rmask, screen->format->Gmask, screen->format->Bmask, screen->format->Amask);
	
	if(toolbox->toolbox_surface == NULL) {
        fprintf(stderr, "SDL_UI_Create_toolbox failed: %s\n", SDL_GetError());
        return NULL;
    }

    toolbox->cols = cols;
    toolbox->rows = rows;
    toolbox->tile_size = tile_size;
    toolbox->posx = 0;
    toolbox->posy = 0;
    toolbox->w = toolbox->cols * toolbox->tile_size;
    toolbox->h = toolbox->rows * toolbox->tile_size;
    toolbox->visible = YES;

    // Fill background color
    SDL_FillRect(toolbox->toolbox_surface, NULL, color);

    // Draw border toolbox window
    // set line border color
    color_border_line = SDL_MapRGB(screen->format, 255, 255, 255);
    SDL_Draw_rect(toolbox->toolbox_surface, color_border_line, 0, 0, cols * tile_size - 1, rows * tile_size - 1);
    // Draw line to window title
    SDL_Draw_line(toolbox->toolbox_surface, color_border_line, 0, toolbox->tile_size, toolbox->cols * toolbox->tile_size, toolbox->tile_size);

 	return toolbox;
}

void SDL_UI_Draw_widgets(SDL_Surface *screen){
	if (main_toolbox->visible == NO) return;
	SDL_Rect rect;
	
	rect.x = main_toolbox->posx;
	rect.y = main_toolbox->posy;
	rect.w = main_toolbox->cols * main_toolbox->tile_size;
	rect.h = main_toolbox->rows * main_toolbox->tile_size;

 	SDL_BlitSurface(main_toolbox->toolbox_surface, NULL, screen, &rect);
}

SDL_Surface* SDL_UI_CreateRGBSurface(SDL_Surface *screen){
	SDL_Surface *tempscreen;

	tempscreen = SDL_CreateRGBSurface(SDL_SWSURFACE, screen->w, screen->h, screen->format->BitsPerPixel, screen->format->Rmask, 
 		screen->format->Gmask, screen->format->Bmask, screen->format->Amask);
 	SDL_BlitSurface(screen, NULL, tempscreen, NULL);

 	return tempscreen;
} 

int SDL_UI_Draw_rect(SDL_Surface *screen, Uint32 color){
 	SDL_Event event;
 	int xi, yi;
 	SDL_Surface *tempscreen;

 	tempscreen = SDL_UI_CreateRGBSurface(screen); 

 	if (tempscreen == NULL) {
 		printf ("Error CreateRGBSurface, Null\n");
 	}

// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit rect mode 
 							return 0;
 							break;
 					}
					break;

				// Capture initial point to draw the rect
				case SDL_MOUSEBUTTONDOWN:
					xi = event.button.x;
					yi = event.button.y;
	 				break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);
						SDL_Draw_rect(screen, color, xi, yi, event.motion.x, event.motion.y);
						// Blit canvas screen to main screen
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 			break;

	 			// Finish draw rect with mouse
	 			case SDL_MOUSEBUTTONUP:
	 				SDL_BlitSurface(screen, NULL, tempscreen, NULL);
					break;
				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 	} while (1);
}


int SDL_UI_Draw_elipse(SDL_Surface *screen, Uint32 color){
 	SDL_Event event;
 	int xc, yc, rx, ry;
 	SDL_Surface *tempscreen;

	tempscreen = SDL_UI_CreateRGBSurface(screen); 	

 	if (tempscreen == NULL) {
 		printf ("Error CreateRGBSurface, Null\n");
 	}
 	// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit elipse mode 
 							return 0;
 							break;
 					}
					break;

				// Capture initial point to draw the elipse
				case SDL_MOUSEBUTTONDOWN:
					xc = event.button.x;
					yc = event.button.y;
	 				break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						rx = abs(event.motion.x - xc); 
						ry = abs(event.motion.y - yc); 
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);
						SDL_Draw_elipse(screen, color, xc, yc, rx, ry);
						// Blit canvas screen to main screen
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 			break;

	 			// Finish draw elipse with mouse
	 			case SDL_MOUSEBUTTONUP:
	 				SDL_BlitSurface(screen, NULL, tempscreen, NULL);
					break;

				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 	} while (1);

}


int SDL_UI_Draw_circle(SDL_Surface *screen, Uint32 color){
 	int xc, yc, r;
 	SDL_Event event;
 	SDL_Surface *tempscreen;

	tempscreen = SDL_UI_CreateRGBSurface(screen); 	

 	if (tempscreen == NULL) {
 		printf ("Error CreateRGBSurface, Null\n");
 	}
 	// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit circle mode 
 							return 0;
 							break;
 					}
					break;

				// Capture initial point to draw the circle
				case SDL_MOUSEBUTTONDOWN:
					xc = event.button.x;
					yc = event.button.y;
	 				break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
		 			   r = (event.motion.x - xc) + (yc - event.motion.y); 
		               SDL_BlitSurface(tempscreen, NULL, screen, NULL);
					   SDL_Draw_circle(screen, color, xc, yc, r);
					   // Blit canvas screen to main screen
					   SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 			break;

	 			// Finish draw circle with mouse
	 			case SDL_MOUSEBUTTONUP:
	 				SDL_BlitSurface(screen, NULL, tempscreen, NULL);
					break;

				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 	} while (1);

}

int SDL_UI_Draw_line(SDL_Surface *screen, Uint32 color){
 	int xi, yi;
 	SDL_Event event;
 	SDL_Surface *tempscreen;

 	tempscreen = SDL_UI_CreateRGBSurface(screen); 

 	if (tempscreen == NULL) {
 		printf ("Error CreateRGBSurface, Null\n");
 	}

// Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
		 			   	SDL_FreeSurface(tempscreen);
 						// Exit line mode 
 							return 0;
 							break;
 					}
					break;

				// Capture initial point to draw the line
				case SDL_MOUSEBUTTONDOWN:
					xi = event.button.x;
					yi = event.button.y;
	 				break;

				// Capture mouse motion
				case SDL_MOUSEMOTION:
		 			if (event.motion.state & SDL_BUTTON_LEFT)
		 			{
						SDL_BlitSurface(tempscreen, NULL, screen, NULL);
						SDL_Draw_line(screen, color, xi, yi, event.motion.x, event.motion.y);
						// Blit canvas screen to main screen
						SDL_BlitSurface(screen, NULL, main_screen, NULL);
		 			}
		 			break;

	 			// Finish draw line with mouse
	 			case SDL_MOUSEBUTTONUP:
	 				SDL_BlitSurface(screen, NULL, tempscreen, NULL);
					break;
					
				default:
				break;
			}
 		}
 		SDL_UI_Draw_widgets(main_screen);
 		SDL_Flip(main_screen);
 		SDL_Delay (16);
 	} while (1);
}