 /* algoritmos_graficas.c
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

 #include <stdio.h>
 #include <stdlib.h>
 #include <SDL.h>
 #include "SDL_Draw.h"

 SDL_Surface *screen;

  void do_middle_point_elipse()
 {
 	int xc,yc,rx,ry;
 	printf("\n-----Algoritmo del punto medio para elipses-----\n");
 	printf("Dame X del punto central el elipse\n");
 	scanf("%i",&xc);
	printf("Dame Y del punto central el elipse\n");
 	scanf("%i",&yc);
 	printf("Dame el radio1 x\n");
 	scanf("%i",&rx);
 	printf("Dame el radio1 y\n");
 	scanf("%i",&ry);
 	
 	SDL_Draw_elipse(screen,xc,yc,rx,ry); 
 }

 void do_middle_point_circle()
 {
 	int x,y,r;
 	printf("\n-----Algoritmo del punto medio para círculos-----\n");
 	printf("Dame X del punto central el círculo\n");
 	scanf("%i",&x);
	printf("Dame Y del punto central el círculo\n");
 	scanf("%i",&y);
 	printf("Dame el radio\n");
 	scanf("%i",&r);
 	
 	SDL_Draw_circle(screen,x,y,r);
 
 }

 void do_bresenham()
 {
 	int xi,xf,yi,yf; // 'x' & 'y' init and final points.

 	printf("\n-----Bresenham-----\n");
 	printf("Dame X del primer punto\n");
 	scanf("%i",&xi);
 	printf("Dame Y del primer punto\n");
 	scanf("%i",&yi);
 	printf("Dame X del segundo punto\n");
 	scanf("%i",&xf);
 	printf("Dame Y del segundo punto\n");
 	scanf("%i",&yf);

 	SDL_Draw_line(screen,xi,yi,xf,yf);

 }

 int check_option_user(int option)
 {
 	switch(option){
 		case 1:
 			//DDA
 			return 0;
 			break;
 		
 		case 2:
 			do_bresenham();
 			return 0;
 			break;

 		case 3:
 			do_middle_point_circle();
 			return 0;
 			break;

		case 4:		
 			do_middle_point_elipse();
 			return 0;
 			break;
 		
 		default:
 			printf("\nOpción incorrecta.");
 			return 1;
 	}	
 }

 void show_menu()
 {
 	int option;
 	printf("\n¿Cual algoritmo ?\n");
 	printf("--Línea--\n");
 	printf("1-DDA (Algoritmo de Análisis Diferencial Digital)\n");
 	printf("2-Bresenham\n");
 	printf("3-Punto medio para círculos\n");
 	printf("4-Punto medio para elipses\n");
 	printf("Opción:");
 	scanf("%i",&option);
 	check_option_user(option);
 }

int SDL_setup()
{
 		if (SDL_Init (SDL_INIT_VIDEO) < 0) {
			fprintf (stderr, "Error SDL_Init.\n");
			return 1;
 		}
 
 		if ((screen = SDL_SetVideoMode (800, 600, 24, 0)) == NULL) {
 			fprintf (stderr, "Error SDL_SetVideoMode.\n");
 			return 1;
 		}
 		return 0;
}

 int main (int argc, char *argv[]) {
 	SDL_Event event;
 	SDLKey key;

 	if(SDL_setup() > 0)
 		return 1;

 	show_menu();

  // Event loop
 	do {
 	SDL_Flip(screen);
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {
 				case SDL_QUIT:
 					SDL_Quit ();
 					return 0;
 				break;
 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
 						SDL_Quit ();
 						return 0;
 						break;
 					}
 				break;
 			}
 		} 
 
 	} while (1);
 	return 0;
 }