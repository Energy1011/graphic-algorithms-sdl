 /* SDL_Paint.c
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

 #include <stdio.h>
 #include <stdlib.h>
 #include <SDL.h> 
 #include <SDL_image.h>
 #include "SDL_Draw.h"
 #include "SDL_UI_Draw.h"

 #define SCREEN_WIDTH 800
 #define SCREEN_HEIGHT 600

// Main screen
SDL_Surface *screen;
// Canvas drawable
SDL_Surface *screen_canvas;
SDL_UI_toolbox *main_toolbox;
// RGB Colors
int R, G, B;
Uint32 color;

enum {
	IMG_BUTTON_CLOSE_UP,
	IMG_BUTTON_LINE_UP,
	IMG_BUTTON_CIRCLE_UP,
	IMG_BUTTON_ELIPSE_UP,
	IMG_BUTTON_RECT_UP,
	IMG_BUTTON_POLY_UP,
	IMG_BUTTON_SPLINE_UP,
	NUM_IMG_BUTTONS
};

const char *img_toolbar_buttons_names [NUM_IMG_BUTTONS] = {
"images/button-close-up.png",
"images/button-line-up.png",
"images/button-circle-up.png",
"images/button-elipse-up.png",
"images/button-rect-up.png",
"images/button-poly-up.png",
"images/button-spline-up.png"
};

SDL_Surface *img_toolbar_buttons[NUM_IMG_BUTTONS];


void Setup_buttons(){
	int e;
	SDL_Surface *load;
	// Load buttons
		for (e = 0; e < NUM_IMG_BUTTONS; e++) {
			load = IMG_Load (img_toolbar_buttons_names [e]);
			
			if (load == NULL) {
				fprintf (stderr, "Error al cargar la imagen %s\n", img_toolbar_buttons_names [e]);
				SDL_Quit ();
				exit (1);
			}
			
			img_toolbar_buttons[e] = load;
		}

}

int Setup_UI(){	
	Uint32 color_toolbox;

	// Main toolbar box
	// Set background color
	color_toolbox = SDL_MapRGB(screen->format,23, 189, 231);
	// Create a tool box: 3 columns * 9 rows with 25 pixels by tile 
	main_toolbox = SDL_UI_Create_toolbox(screen, color_toolbox, 3, 9, 25);
	Setup_buttons();
		 											// colum = 2 , row = 0
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[0], 2, 0);
		// Line button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[1], 0, 2);
		// Circle button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[2], 1, 2);
		// Elipse button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[3], 0, 3);
		// Rect button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[4], 1, 3);
		// Poly button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[5], 0, 4);
		// Spline button
		SDL_UI_Attach_icon(main_toolbox, img_toolbar_buttons[6], 1, 4);


 	return 0;
}


int SDL_setup(){
	Uint32 color_bg;		

	if (SDL_Init (SDL_INIT_VIDEO) < 0) {
		fprintf (stderr, "Error SDL_Init.\n");
	return 1;
	}

	if ((screen = SDL_SetVideoMode (SCREEN_WIDTH, SCREEN_HEIGHT, 24, 0)) == NULL) {
		fprintf (stderr, "Error SDL_SetVideoMode.\n");
		return 1;
	}

	// Create layer screen to canvas drawable
	screen_canvas =  SDL_UI_CreateRGBSurface(screen); 

	// Set screen_canvas bgcolor to white
	color_bg = SDL_MapRGB(screen_canvas->format, 255, 255, 255);
    SDL_FillRect(screen_canvas, NULL, color_bg);

	SDL_UI_Set_main_screen(screen);

	return 0;
}

int main (int argc, char *argv[]){
 	SDL_Event event;
 	
 	// init the color to draw

 	if(SDL_setup() > 0) return 1;

 	if( Setup_UI() > 0) return 1;
 		
 	color = SDL_MapRGB(screen->format, R, G, B); 

	printf("\nPress key to change mode:\n");
	printf("l = Line\n");
	printf("c = Circle\n");
	printf("e = Elipse\n");
	printf("r = Rect\n");
	printf("F1 = Show toolbar\n");


  // Event loop
 	do {
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {
 				
 				case SDL_QUIT:
 					SDL_Quit ();
 					return 0;
 				break;

 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
 						SDL_Quit ();
 						return 0;
 						break;

						case SDLK_l:
 						printf("*Mode: Line\n");
 						SDL_UI_Draw_line(screen_canvas, color);
 						break;

 						case SDLK_c:
 						printf("*Mode: Circle\n");
 						SDL_UI_Draw_circle(screen_canvas, color);
 						break;

 						case SDLK_e:
 						printf("*Mode: Elipse\n");
 						SDL_UI_Draw_elipse(screen_canvas, color);
 						break;

 						case SDLK_r:
 						printf("*Mode: Rect\n");
 						SDL_UI_Draw_rect(screen_canvas, color);
 						break;

 						case SDLK_F1:
 						// Turn invisible
	 					main_toolbox->visible = YES;
 						break;

 						default:
 						break;
 					}
 				break;

				case SDL_MOUSEMOTION:
		 			// Check if MouseOver MainToolbox
	 				if(SDL_UI_MouseOver(main_toolbox->posx,
										 main_toolbox->posy,
		  								 main_toolbox->w,
		  								 main_toolbox->h,
		  								 event.motion.x,
		  								 event.motion.y ) > 0){

	 					// Check for dragging MianToolbox
			 			if( SDL_GetMouseState(0, 0) & SDL_BUTTON(1))
			 			{
			 					// Erase the screen with canvas before drag to don't show toolbox last position
			 					SDL_BlitSurface(screen_canvas, NULL, screen, NULL);
			 					// Init the drag
			 					SDL_UI_Drag_toolbox(screen, main_toolbox);
			 			}	 


		 			}
		 		break;

		 		case SDL_MOUSEBUTTONUP:
	 		// Check for toolbar buttons MouseOver
	 			if(SDL_UI_MouseOver(main_toolbox->posx + (main_toolbox->tile_size * 2),
									 main_toolbox->posy,
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
	 				// Turn invisible
	 				main_toolbox->visible = NO;
	  			}		

	  			// Line button
	  			if(SDL_UI_MouseOver(main_toolbox->posx,
									 main_toolbox->posy + (main_toolbox->tile_size * 2),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Line\n");
					SDL_UI_Draw_line(screen_canvas, color);

	  			}

	  			// Circle Button
	  			if(SDL_UI_MouseOver(main_toolbox->posx + main_toolbox->tile_size,
									 main_toolbox->posy + (main_toolbox->tile_size * 2),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Circle\n");
					SDL_UI_Draw_circle(screen_canvas, color);

	  			}

	  			// Elipse button
	  			if(SDL_UI_MouseOver(main_toolbox->posx,
									 main_toolbox->posy + (main_toolbox->tile_size * 3),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Elipse\n");
					SDL_UI_Draw_elipse(screen_canvas, color);

	  			}

	  			// Rect button
	  			if(SDL_UI_MouseOver(main_toolbox->posx + main_toolbox->tile_size,
									 main_toolbox->posy + (main_toolbox->tile_size * 3),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Rect\n");
					SDL_UI_Draw_rect(screen_canvas, color);

	  			}			

	  			// Polygon button
	  			if(SDL_UI_MouseOver(main_toolbox->posx,
									 main_toolbox->posy + (main_toolbox->tile_size * 4),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Poly\n");
					SDL_UI_Draw_polygon(screen_canvas, color, 5);
	  			}

	  			// Spline button
	  			if(SDL_UI_MouseOver(main_toolbox->posx + main_toolbox->tile_size,
									 main_toolbox->posy + (main_toolbox->tile_size * 4),
	  								 25,
	  								 25,
	  								 event.motion.x,
	  								 event.motion.y ) > 0){		
 					printf("*Mode: Spline\n");
 					SDL_UI_Draw_spline(screen_canvas, color);
	  			}
	  			break;

	  			default:
	  			break;

 			}
 		} 

 	// Blit canvas to screen
	SDL_BlitSurface(screen_canvas, NULL, screen, NULL);
	// Blit widgets to screen
	SDL_UI_Draw_widgets(screen);
	// Main flip to screen
 	SDL_Flip(screen);
 	} while (1);
 	return 0;
 }