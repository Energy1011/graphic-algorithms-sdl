 /* draw_penta_star.c
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#include <stdio.h>
#include <SDL.h>
#include "SDL_Draw.h"

int points [9][4]= {
{10, 10, 190, 10},
{190, 10, 190, 140},
{190, 140, 10, 140},
{10, 140, 10, 10},
{20, 20, 100, 130},
{100, 130, 180, 20},
{180, 20, 15, 100},
{15, 100, 185, 100},
{185, 100, 20, 20}
}; 

SDL_Surface *screen;

int do_star()
{
 int e=0;
 int stop;

 for (e = 0; e <9; e++)
 {
 	//invert Y
 	points[e][1] = 600 - points[e][1];
 	points[e][3] = 600 - points[e][3];
 	SDL_Draw_line(screen,points[e][0],points[e][1],points[e][2],points[e][3]);

 }

}

int SDL_setup()
{
	 		if (SDL_Init (SDL_INIT_VIDEO) < 0) {
 			fprintf (stderr, "Error SDL_Init.\n");
 			return 1;
 		}
 
 		if ((screen = SDL_SetVideoMode (800, 600, 24, 0)) == NULL) {
 			fprintf (stderr, "Error SDL_SetVideoMode.\n");
 			return 1;
 		}
 		return 0;
}

 int main (int argc, char *argv[]) {
 	SDL_Event event;
 	SDLKey key;

 	if(SDL_setup() > 0)
 		return 1;
 	do_star();
  // Event loop
 	do {
 	SDL_Flip(screen);
 		while (SDL_PollEvent (&event) > 0) {
 			switch (event.type) {
 				case SDL_QUIT:
 					SDL_Quit ();
 					return 0;
 				break;
 				case SDL_KEYDOWN:
 					switch(event.key.keysym.sym){
 						case SDLK_ESCAPE:
 						SDL_Quit ();
 						return 0;
 						break;
 					}
 				break;
 			}
 		} 
 
 	} while (1);
 	return 0;
 }