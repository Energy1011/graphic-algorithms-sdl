 /* SDL_Draw.c
 * This file is part of Prácticas de Gráficas
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Prácticas de Gráficas is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Prácticas de Gráficas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Prácticas de Gráficas; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <math.h>
#include <SDL.h>
#include "SDL_Draw.h"

void SDL_Draw_spline(SDL_Surface *screen, Uint32 color, int x[4], int y[4]){
    double time;
    int xf,yf;

    for(time= 0; time<=1.0; time +=0.001)
    {   
        xf=pow(1-time,3) * x[0] +
         3 * time * pow(1-time, 2) * x[1] +
         3*pow(time,2)*(1-time) * x[2] +
         pow(time,3) * x[3];

        yf=pow(1-time,3) * y[0] +
        3 * time * pow(1-time,2) * y[1] +
        3 * pow(time,2) * (1-time) * y[2] +
        pow(time,3) * y[3];

        SDL_Draw_pixel(screen, color, xf, yf);
    }
}

SDL_point SDL_set_vertex_polygon(int xc, int yc, int r, float radian, float angle){
    SDL_point point;

    point.x = xc + ceil(r * cos(angle/radian));
    point.y = yc + ceil(r * sin(angle/radian));

    return point;
}

void SDL_Draw_polygon(SDL_Surface *screen, Uint32 color, SDL_point *points, int sides){
    int e;

    points[sides].x = points[0].x;
    points[sides].y = points[0].y;

    for (e = 0; e < sides; e++)
    {   
        SDL_Draw_line(screen, color, points[e].x, points[e].y, points[e+1].x, points[e+1].y);
    }

}

void SDL_Draw_rect(SDL_Surface *screen, Uint32 color, int xi, int yi, int xf, int yf){
    // top
    SDL_Draw_line(screen, color, xi, yi, xf, yi);
    // right
    SDL_Draw_line(screen, color, xf, yi, xf, yf);
    // bottom
    SDL_Draw_line(screen, color, xf, yf, xi, yf);
    // left    
    SDL_Draw_line(screen, color, xi, yf, xi, yi);

}

void mirror_plot_point_elipse(SDL_Surface *screen, Uint32 color, int xc, int yc, int x, int y){
    SDL_Draw_pixel(screen, color, xc + x, yc + y);
    SDL_Draw_pixel(screen, color, xc - x, yc + y);
    SDL_Draw_pixel(screen, color, xc + x, yc - y);
    SDL_Draw_pixel(screen, color, xc - x, yc - y);
}

void SDL_Draw_elipse(SDL_Surface *screen, Uint32 color, int xc, int yc, int rx, int ry){
    if (ry < 0) ry *= -1;
    float x = 0,y = ry,rx2,ry2,p;
    rx2 = pow(rx,2);
    ry2 = pow(ry,2);
    p = ry2 - (rx2 * ry) + (0.25 * rx2);

    // initials points
    SDL_Draw_pixel(screen, color, xc, yc + ry);
    SDL_Draw_pixel(screen, color, xc , yc - ry);

    while((ry2 * x) < (rx2 * y))
      {
      if(p<0)
         { x++;
           p += (2 * ry2 * x) + ry2;
         }
      else
         {
           x++; y--;
           p += (2 * ry2 * x) - (2 * rx2 * y) + ry2;
         }
         mirror_plot_point_elipse(screen, color, xc, yc, x, y);
      }

    p = (ry2) * pow((x+0.5),2) + (rx2) * pow((y-1),2) - (rx2 * ry2);

    while(y > 0)
      {
        y--;     
         if (p>0)
         {
           p = p - (2 * rx2 * y) + rx2;
         }
         else
         {
           x++;
           p += (2 * ry2 * x) - (2 * rx2 * y) + rx2;
         }
            mirror_plot_point_elipse(screen, color, xc, yc, x, y);
      }
}

void mirror_plot_point(SDL_Surface *screen, Uint32 color, int xc, int yc, int x, int y, int r){

// top
    SDL_Draw_pixel(screen, color, xc + x, yc - y);
    SDL_Draw_pixel(screen, color, xc - x, yc - y);

// right
    SDL_Draw_pixel(screen, color, xc + y, yc - x);
    SDL_Draw_pixel(screen, color, xc + y, yc + x);

// bottom
    SDL_Draw_pixel(screen, color, xc + x, yc + y);
    SDL_Draw_pixel(screen, color, xc - x, yc + y);

// left
    SDL_Draw_pixel(screen, color, xc - y, yc + x);
    SDL_Draw_pixel(screen, color, xc - y, yc - x);

// middles
    SDL_Draw_pixel(screen, color, xc + r, yc);
    SDL_Draw_pixel(screen, color, xc - r, yc);
    SDL_Draw_pixel(screen, color, xc, yc + r);
    SDL_Draw_pixel(screen, color, xc, yc - r);
}

void SDL_Draw_circle(SDL_Surface *screen, Uint32 color, int xc, int yc, int r){
    int p;
    int x = 0,y = r;
        
    // Negative radio
    if (r < 0)
    {
        r = r * -1;
    }

    p = 1 - r;

    while (x < y)
    {  
        x++;
        if (p < 0 )
        {
            p = p + 2*x + 1;
        }else
        {
             y--;
             p = p + 2*(x - y) + 1;
        }
        mirror_plot_point(screen, color, xc, yc, x, y, r);
    }

}

void SDL_Draw_line(SDL_Surface *screen, Uint32 color, int x1, int y1, int x2, int y2) {
    int delta_x;
    int delta_y;
    int double_delta;
    int dif_delta;
    int p;
    int swapx, swapy;
    int increment;
    
    delta_x = x2 - x1;
    delta_y = y2 - y1;
    
    if ((delta_x > 0 && delta_y > 0) || (delta_x < 0 && delta_y < 0)) {
        increment = 1;
    } else {
        increment = -1;
    }
    
    delta_x = abs (delta_x);
    delta_y = abs (delta_y);
    
    if (delta_x > delta_y) {
        /* Switch points */
        if (x1 > x2) {
            swapx = x2;
            swapy = y2;
            x2 = x1;
            y2 = y1;
            x1 = swapx;
            y1 = swapy;
        }
        double_delta = delta_y * 2;
        dif_delta = double_delta - (2 * delta_x);
        p = double_delta - delta_x;
        
        SDL_Draw_pixel(screen, color, x1, y1);
        while (x1 < x2) {
            x1++;
            
            if (p < 0) {
                p = p + double_delta;
            } else {
                p = p + dif_delta;
                y1 = y1 + increment;
            }
            SDL_Draw_pixel(screen, color, x1, y1);
        }
    } else {
        /* Switch points */
        if (y1 > y2) {
            swapx = x2;
            swapy = y2;
            x2 = x1;
            y2 = y1;
            x1 = swapx;
            y1 = swapy;
        }
        double_delta = delta_x * 2;
        dif_delta = double_delta - (2 * delta_y);
        p = double_delta - delta_y;
        
        SDL_Draw_pixel(screen, color, x1, y1);
        while (y1 < y2) {
            y1++;
            
            if (p < 0) {
                p = p + double_delta;
            } else {
                p = p + dif_delta;
                x1 = x1 + increment;
            }
            SDL_Draw_pixel(screen, color, x1, y1);
        }
    }
}

void SDL_Draw_pixel (SDL_Surface *screen, Uint32 color, int x, int y) {
    void *pixel;
    int bpp;
    if (x < 0 || y < 0 || y > screen->h || x > screen->w) return;
    
    bpp = screen->format->BytesPerPixel;
    pixel = screen->pixels + (y * screen->pitch) + (x * bpp);
    
    SDL_LockSurface (screen);
    switch (bpp) {
        case 1:
            *(Uint8 *) pixel = (Uint8) color;
            break;
        case 2:
            *(Uint16 *) pixel = (Uint16) color;
            break;
        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                ((Uint8 *) pixel)[0] = (color >> 16) & 0xFF;
                ((Uint8 *) pixel)[1] = (color >> 8) & 0xFF;
                ((Uint8 *) pixel)[2] = color & 0xFF;
            } else {
                ((Uint8 *) pixel)[2] = (color >> 16) & 0xFF;
                ((Uint8 *) pixel)[1] = (color >> 8) & 0xFF;
                ((Uint8 *) pixel)[0] = color & 0xFF;
            }
            break;
        case 4:
            *(Uint32 *) pixel = (Uint32) color;
            break;
    }
    SDL_UnlockSurface (screen);
}